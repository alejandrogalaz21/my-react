import React from 'react'
import { Button, Modal as ModalB, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

class Modal extends React.Component {

  state = { modal: false }

  toggle = () => this.setState({ modal: !this.state.modal })

  render() {
    const closeBtn = <button className="close" onClick={this.toggle}>&times;</button>
    return (
      <div>
        <Button color="info" onClick={this.toggle}>{this.props.buttonLabel} Click</Button>
        <ModalB isOpen={this.state.modal} toggle={this.toggle} className={`${this.props.className} modal-xl`} >
          <ModalHeader toggle={this.toggle} close={closeBtn}>Modal title</ModalHeader>
          <ModalBody>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
            fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
            mollit anim id est laborum.
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.toggle}>Do Something</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </ModalB>
      </div>
    )
  }
}

export default Modal