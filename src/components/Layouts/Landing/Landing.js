import React, { Component, Fragment } from 'react'
import Header from './../Landing/Header'
import Section from './../Landing/Section'
import Navigation from './../Landing/Navigation'
import Footer from './../Landing/Footer'

export default class Landing extends Component {
  render() {
    return (
      <Fragment>
        <Header />
        <Section />
        <Footer />
      </Fragment>
    )
  }
}
