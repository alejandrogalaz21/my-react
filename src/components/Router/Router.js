import routes from './routes'
import React, { Component } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
// import NotFoundPage from '../components/404/NotFoundPage'
// import PrivateRoute from './PrivateRoute'
export default class Router extends Component {
  render() {
    const routesList = routes.map((r, i) => (<Route key={`${r.name}-route-${i}`} exact path={r.path} component={r.component} />))
    return (
      <BrowserRouter>
        <Switch>
          {routesList}
        </Switch>
      </BrowserRouter>
    )
  }
}
