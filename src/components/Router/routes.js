import Landing from './../Layouts/Landing/Landing'
import Dashboard from './../Layouts/Dashboard/Dashboard'
export default [
  { name: 'Home', path: '/', component: Landing },
  { name: 'Dashboard', path: '/dashboard', component: Dashboard },
]