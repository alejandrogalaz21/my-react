import React, { Component } from 'react'
import Router from './components/Router/Router'
class App extends Component {
  render() {
    return <Router />
  }
}

export default App