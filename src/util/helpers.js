//  ____                          _       _              _                             
// |  _ \    ___    __ _    ___  | |_    | |__     ___  | |  _ __     ___   _ __   ___ 
// | |_) |  / _ \  / _` |  / __| | __|   | '_ \   / _ \ | | | '_ \   / _ \ | '__| / __|
// |  _ <  |  __/ | (_| | | (__  | |_    | | | | |  __/ | | | |_) | |  __/ | |    \__ \
// |_| \_\  \___|  \__,_|  \___|  \__|   |_| |_|  \___| |_| | .__/   \___| |_|    |___/
//                                                          |_|                        








//
// ░I░N░P░U░T░S░ ░H░E░L░P░E░R░S░

//  HANDLE WHEN INPUTS CHANGE 
export const onChangeState = event => {
  const target = event.target
  const name = target.name
  const value = target.type === 'checkbox' ? target.checked : target.value
  const hasLevel = name.includes('.')
  if (hasLevel) {
    const props = name.split('.')
    const childs = name.split('.').length
    switch (childs) {
      case 2:
        return (state, prop) => ({ [props[0]]: { ...state[props[0]], [props[1]]: { ...state[props[0]][props[1]], value } } })
      case 3:
        return (state, prop) => ({ [props[0]]: { ...state[props[0]], [props[1]]: { ...state[props[0]][props[1]], [props[2]]: { ...state[props[0]][props[1]][props[2]], value } } } })
    }
  }
  // no childs
  return (state, prop) => ({ [name]: { ...state[name], value } })
}

// cuando camabia el componente select data custom.
export const handleChangeSelectData = ({ name, value }) => {
  const hasLevel = name.includes('.')
  if (hasLevel) {
    const props = name.split('.')
    const childs = name.split('.').length
    switch (childs) {
      case 2:
        return (state, prop) => ({ [props[0]]: { ...state[props[0]], [props[1]]: { ...state[props[0]][props[1]], value } } })
      case 3:
        return (state, prop) => ({ [props[0]]: { ...state[props[0]], [props[1]]: { ...state[props[0]][props[1]], [props[2]]: { ...state[props[0]][props[1]][props[2]], value } } } })
    }
  }
  // no childs
  return (state, prop) => ({ [name]: { ...state[name], value } })
}

//
// ░C░R░U░D░ ░H░E░L░P░E░R░S░

const formater = list => JSON.stringify(list, null, 2);
// ( C ) Add a new item to the Array
const addItem = (list, item) => [...list, item];
// ( R ) Find item by id from a Array
const findItem = (list, _id) => list.find(i => i._id === _id);
// ( U ) Update item from a Array
const updatedItem = (list, item) => {
  const index = list.findIndex(i => i._id === item._id);
  return [...list.slice(0, index), item, ...list.slice(index + 1)];
}
// ( D ) Deletes a item from a Array
const deleteItem = (list, _id) => list.filter(i => i._id !== _id);
// Generates a random number
const generateId = () => Math.floor(Math.random() * 1000)
// Will return a number inside the given range, inclusive of both minimum and maximum
// i.e. if min=0, max=20, returns a number from 0-20
const getRandomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min



//
// ░V░A░L░I░D░A░T░I░O░N░S░ ░H░E░L░P░E░R░S░

// CHECK VALIDATION ON SUBMIT
export const specialValidations = (state, valitions, prop = null) => {
  let stateValidated = {}
  const arrayState = Object.keys(state).map(key => ({ field: key, value: state[key].value }))
  for (let ob of arrayState) {
    const validation = valitions.find(v => v.field === ob.field)
    if (validation) {
      const r = validation.function(ob.value, state)
      stateValidated = { ...stateValidated, [ob.field]: { ...state[ob.field], ...r } }
    }
  }
  const isValid = validateState(stateValidated)
  return { isValid, stateValidated }
}

// CHECK VALIDATION WHEN A INPUT CHANGE HES VALUE
export const specialValidationsOnChange = (state, valitions, prop) => {
  let stateValidated = {}
  const props = prop.split('.')
  const propName = props[props.length - 1]
  const arrayState = Object.keys(state).map(key => ({ field: key, value: state[key].value }))
  const field = arrayState.find(f => f.field === propName)
  if (field) {
    const validation = valitions.find(v => v.field === field.field)

    if (validation) {

      const r = validation.function(field.value, state)

      stateValidated = { ...state, [field.field]: { ...state[field.field], ...r } }
    }
    const isValid = validateState(stateValidated)
    return { isValid, stateValidated }
  }
  return null
}

// check if empty or not 
export const isEmpty = value => (
  value === undefined ||
  value === null ||
  (typeof value === 'object' && Object.keys(value).length === 0) ||
  (typeof value === 'string' && value.trim().length === 0)
)

export const checkIsEmpty = value => isEmpty(value) ? isEmpty(value) : value


// checka si todos los elementos del state tienen la  
//  propiedad valid === true devuelve un Boolean.
export const validateState = s => Object.keys(s).map(k => ({ ...s[k] })).filter(o => o.valid !== null).every(o => o.valid === true)
